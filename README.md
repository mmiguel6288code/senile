
senile
==================================================
[![PyPI version](https://badge.fury.io/py/senile.svg)](https://badge.fury.io/py/senile)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

A command-line todo list and time tracker.

Overview
-------------------------

This utility functions as a personal todo list and time tracker. You can create tasks with tags and they are saved in sqlite3 file: `~/.senile`.

Installation
-------------------------

### install from pypi (recommend)
`pip3 install senile`

### install from git (latest master)
`pip3 install -U git+https://gitlab.com/nul.one/senile.git`

Usage
-------------------------

Commands:

- `add` Add a new task.
- `modify` Modify existing task.
- `remove` Delete following task(s).
- `list` List tasks.
- `archive` Same as list but show archived tasks.
- `start` Start following task.
- `stop` Stop tasks.
- `todo` Set following task(s) to todo.
- `done` Set following task(s) to done.
- `hide` Archive following task(s).
- `info` Show detailed info of a task.
- `tags` List all tags and their counts.
- `note` Open task notes for editing.

### add

Add a new task.

- aliases: a
- usage: `senile add Some text description +tag1 +tag2 +tag3`
- example: `senile add Submit the project +work +project +new`

### modify

Modify existing task.

- aliases: mod, m
- usage: `senile mod ID_OR_UUID Updated text -TAG_TO_REMOVE +TAG_TO_ADD`
- example: `senile mod 5 Re-submit the finished project -new +old`


### remove

Delete following task(s).

- aliases: rm
- usage: `senile rm ID_OR_UUID_1 ID_OR_UUID_2 ID_OR_UUID_3`
- example: `senile rm d59de172 fcf8799e 4 5`

### list

List tasks.

- aliases: ls, l
- usage: `senile l SOME TEXT TO SEARCH +CONTAIN_TAG -DO_NOT_CONTAIN_TAG`
- example: `senile l project +work -old`

### archive

Same as list but show archived tasks.

- aliases: ar, arc, arch
- usage: `senile ar SOME TEXT TO SEARCH +CONTAIN_TAG -DO_NOT_CONTAIN_TAG`
- example: `senile ar project +work -old`

### start

Start following task.

- usage: `senile start TASK_ID_OR_UUID`
- example: `senile start 5`
- note: Only 1 task can be started.

### stop

Stop tasks.

- Called without arguments and stops a single task that was started.

### todo

Set following task(s) to todo.

- usage: `senile todo ID_OR_UUID_1 ID_OR_UUID_2 ID_OR_UUID_3`
- example: `senile todo d59de172 fcf8799e 4 5`

### done

Set following task(s) to done.

- usage: `senile done ID_OR_UUID_1 ID_OR_UUID_2 ID_OR_UUID_3`
- example: `senile done d59de172 fcf8799e 4 5`

### hide

Archive following task(s).

- usage: `senile hide ID_OR_UUID_1 ID_OR_UUID_2 ID_OR_UUID_3`
- example: `senile hide d59de172 fcf8799e 4 5`

### info

Show detailed info of a task.

- aliases: i, inf, show
- usage: `senile show TASK_ID_OR_UUID`
- example: `senile show d59de172`

### tags

List all tags and their counts.

- aliases: t, tag
- Called without arguments. Lists tags of all tasks, even archived.

### notes

Edit notes for a task.

- aliases: note, n usage: `senile note TASK_ID_OR_UUID`
- Opens a multiline prompt where you can edit notes. (Alt+Return to finish)


Configuration
-------------------------

Configuration options are available as environment variables that you need to export, either on each senile command or in your environment file (e.g. `.bashrc`, `.profile`, etc.).

### Data file

By default, senile creates and uses an sqlite3 database file located at `~/.senile`.
Export `SENILE_DB_PATH` variable to desired alternative file location.

### Icons

You can change default status icon theme by exporting `SENILE_ICON_THEME`.
Possible values are:

- `default` - standard theme
    - hidden: `👻`
    - done: `✔️ `
    - todo: `⭕`
    - active: `🔴`
- `text`
    - hidden: `h`
    - done: `d`
    - todo: `T`
    - active: `A`
- `senile`
    - hidden: `👓`
    - done: `⚰️ `
    - todo: `📰`
    - active: `🧠`
- `love`
    - hidden: `💔`
    - done: `💖`
    - todo: `❤️ `
    - active: `💘`
- `xmas`
    - hidden: `☃️ `
    - done: `🎁`
    - todo: `🎄`
    - active: `🛷`
- `archery`
    - hidden: `🐺`
    - done: `🏅`
    - todo: `🎯`
    - active: `🏹`

