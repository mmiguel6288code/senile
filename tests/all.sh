#!/bin/bash
# -*- coding: UTF-8 -*-

set -e
export LC_ALL=C.UTF-8
export LANG=C.UTF-8
script_dir=$(cd `dirname "$0"`; pwd; cd - 2>&1 >> /dev/null)
cd $script_dir

data_path=$script_dir/data_dir/senile_test_data
rm -rf $(dirname $data_path)

[ -d "env" ] && rm -rf env
pip3 install virtualenv
virtualenv -p python3 env 
source env/bin/activate

export SENILE_DB_PATH=$data_path
pip install pytest coverage pytest-cov
pip install -U ../
py.test -vv --cov=senile --cov-report html
coverage report
rm -rf $(dirname $data_path)

